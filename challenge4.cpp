#include<iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int board[10][10];
//GAME STATUS
//0 = START GAME;
//1 = GAME OVER BOT CATCH YOU;
//2 = GAME OVER COVID INFECT;
//3 = GO NEXT LEVLE;
int gameStatus = 0;
int playerInGame = 1;
int botPerLevel = 3;
int covidInit = 1;
int covidSpread = 2;
int totalCovid = covidInit;

//FUNCTION
void resetBoard(int);
void printBoard();
void move(int,int,int,int);
bool canMove(int,int,int,int);
void checkPosition(int);
int randomDirection();
int randomPosition();
int moveX(int,int);
int moveY(int,int);
void botInit(int);
void covidStart(int);
void spread(int);
bool canSpread(int,int,int);
void covidSpreadOut(int,int,int);

int main () {
	srand (time(0));
	

	for(int gameLevel = 1; gameLevel <=3 ; gameLevel++){
		int totalPlayer = playerInGame + (botPerLevel * gameLevel);
		cout<<"GAME ON LEVEL "<<gameLevel<<endl;
		resetBoard(totalPlayer);
		do{
			for(int i =1 ;i<= totalPlayer ; i++) checkPosition(i);
			totalCovid += covidSpread;
			spread(2);
			printBoard();
		}while(gameStatus == 0);
		if(gameStatus == 2 || gameStatus == 1) {
			cout<<"YOU LOSE ";
			if(gameStatus ==1) cout<<"ELVES CATCH YOUR SANTA";
			else cout<<"YOUR SANTA HAS INFECT COVID";
			cout<<endl;
			gameLevel = 666;
		}
		else cout<<"YOUR WIN ON LEVEL "<<gameLevel<<endl;
	}
}

void printBoard() {
	for(int y=0;y<10;y++) {
		for(int x=0 ;x<10 ; x++) {
			//cout<<board[x][y];
			if(board[x][y] != 99) {
				if(board[x][y] == 1) cout<<"S";
				else if(board[x][y] == 0) cout<<" ";
				else if(board[x][y] == 69) cout<<"R";
				else cout<<"E";
			}else cout<<"C";
		}
		cout<<endl;
	}
}

void resetBoard(int totalPlayer){
	gameStatus = 0;
	covidSpread = 1;
	for(int x=0 ;x<10 ; x++) for(int y=0;y<10;y++) board[x][y] = 0;
	//INIT PLAYER AND WINCONDITION
	board[0][0] = 1;
	board[9][9] = 69;
	
	botInit(totalPlayer);
	covidStart(covidInit);
	printBoard();
//	board[6][7] = 2;
//	board[7][3] = 3;
//	board[1][4] = 4;
//	board[5][3] = 99;
}

void botInit(int totalPlayer){
	for(int i = 2 ;i<=totalPlayer;i++){
		int x,y;
		do{
			x = randomPosition();
			y = randomPosition();
		}while(board[x][y] != 0);
		board[x][y] = i;
	}
}

void covidStart(int covid){
	for(int i=0;i<covid;i++){
		int x,y;
		do{
			x = randomPosition();
			y = randomPosition();
		}while(board[x][y] != 0);
		board[x][y] = 99;
	}
}

void covidSpreadOut(int direction,int x,int y){
	int targetX,targetY;
	targetX = moveX(direction,x);
	targetY = moveY(direction,y);
	board[targetX][targetY] = 99;
}

void spread(int total){
	bool founded = false;
	int x=0,y=0;
	while(!founded && x < 10) {
		y=0;
		while(!founded && y < 10){		
		if(board[x][y] == 99) founded = true;
		if(!founded) y++;
		}
		if(!founded) x++;
	}
	if(founded) {
		bool goNext = false;
		int direction = randomDirection();
		int counter = 0;
		while(!canSpread(direction,x,y)){
			direction = randomDirection();	
			if(counter >= 6) {
				goNext = true;
				break;
			}
		}
		if(!goNext) covidSpreadOut(direction,x,y);
	}
}

bool canSpread(int direction,int x,int y){
	int targetX,targetY;
	targetX = moveX(direction,x);
	if(targetX > 9 || targetX < 0) return false;
	targetY = moveY(direction,y);
	if(targetY > 9 || targetY < 0) return false;
	if(board[targetX][targetY]==0) return true;
	else return false; 
}


bool canMove(int direction,int player,int x,int y){
	int targetX,targetY;
	targetX = moveX(direction,x);
	if(targetX > 9 || targetX < 0) return false;
	targetY = moveY(direction,y);
	if(targetY > 9 || targetY < 0) return false;
	
	if(player != 1) {
		//BOT CAN LAND ON SPACE,PLAYER OR COVID BUT CANT LAND ON BOT AND WIN CONDITION
		if(board[targetX][targetY] == 1 || board[targetX][targetY] == 99 || board[targetX][targetY] == 0) {
			if(board[targetX][targetY] == 1) {
				cout<<"BOT CATCHT YOUR SANTA"<<endl;
				gameStatus = 1;
			}
			return true;
		}
		else {
			cout<<"LAND ON BOT MATE"<<endl;
			return false;
		}
	}
	//PLAYER CAN LAND EVRYTHING
	else return true;
	
	return true;
}



int moveX(int direction,int x) {
	if(direction == 1 || direction == 4 || direction == 7) return x - 1;
	if(direction == 2 || direction == 8) return x;
	if(direction == 3 || direction == 6 || direction == 9) return x + 1;
	return x;
}

int moveY(int direction,int y) {
	if(direction >=1 && direction <= 3) return y + 1;
	if(direction ==4 || direction == 6) return y;
	if(direction >=7 && direction <= 9) return y - 1;
	return y;
}

void move(int direction,int player,int x,int y){
	if(canMove(direction,player,x,y)) {
		cout<<"PLAYER: "<<player<<" CAN MOVE TO "<<direction<<endl;
		int targetX,targetY;
		targetX = moveX(direction,x);
		targetY = moveY(direction,y);
		
		if(player == 1){
			if(board[targetX][targetY] != 0){
				if(board[targetX][targetY] == 99) {
				cout<<"COVID INFECTED TO PLAYER: "<<player<<endl<<"GAME OVER COVID INFCLECT TO SANTA";
				gameStatus = 2;
				}else if(board[targetX][targetY] == 69) {
					gameStatus = 3;
				}
				else gameStatus = 1;
			}
		}
		
		if(board[targetX][targetY] == 99) cout<<"COVID INFECTED TO PLAYER: "<<player<<endl;
		else board[targetX][targetY] = player;
		board[x][y] = 0;
//		printBoard();
	}
	else {
		cout<<"CANT MOVE "<<direction<<" LA BRO"<<endl;
		if(player == 1) {
			int direction;
			cin>>direction;
			while(direction<1 || direction>9 || direction == 5){
				cout<<"RE INPUT  ";
				cin>>direction;
			}
			move(direction,player,x,y);
		}
		move(randomDirection(),player,x,y);
	}	
}

void checkPosition(int player) {
	bool founded = false;
	int x=0,y=0;
	while(!founded && x < 10) {
		y=0;
		while(!founded && y < 10){		
		if(board[x][y] == player) founded = true;
		if(!founded) y++;
		}
		if(!founded) x++;
	}
	if(founded) {
		int direction;
		cout<<"PLAYER:"<<player<<" FOUND AT("<<x<<","<<y<<")"<<endl;
		if(player == 1) {
			cin>>direction;
			while(direction<1 || direction>9 || direction == 5){
				cout<<"RE INPUT  ";
				cin>>direction;
			}
			move(direction,player,x,y);
		}else if(player == 99){
			cout<<"COVID"<<endl;
		}else move(randomDirection(),player,x,y);
	}
	else cout<<"PLAYER:"<<player<<" IS NOT FOUND ON BOARD"<<endl;
}

int randomDirection() {
	int number;
	do{
		//random range 1 - 9 no 5
		number =  rand() % 9 + 1; 
	}while( number ==  5);
	return number;
}

int randomPosition() {
	//random 1 - 8 cant 0,0 cause is santa and 9,9 cause WIN CONDITION
	return  rand() % 8 + 1; 
}
